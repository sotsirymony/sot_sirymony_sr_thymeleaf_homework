package com.example.demo.Entity;

import javax.persistence.*;
//Entity With h2 database
@Entity
public class User
{
    @Id
    int id;
    @Column
    String username;
    @Column
    String password;
    @Column
    String authority;

    public User() {
    }

    public User(int id, String username, String password, String authority) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.authority = authority;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String userName) {
        this.username = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String auhority) {
        this.authority = auhority;
    }


}
