package com.example.demo.Controller;

import com.example.demo.Entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class ShowErrorController {
    //Show Error /500
    @GetMapping("/500")
    public String error500(@ModelAttribute User user, ModelMap modelMap)
    {
        modelMap.addAttribute("user", user);
        return "/error/403";
    }
    //show Error/403
    @GetMapping("/403")
    public String error403(@ModelAttribute User user, ModelMap modelMap)
    {
        modelMap.addAttribute("user", user);
        return "/error/403";
    }

    //show Error /404
    @GetMapping("/404")
    public String error404(@ModelAttribute User user, ModelMap modelMap)
    {
        modelMap.addAttribute("user", user);
        return "/error/403";
    }

}
