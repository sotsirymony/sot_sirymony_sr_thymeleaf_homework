package com.example.demo.Controller;

import com.example.demo.Entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class ShowloginController
{

    @GetMapping("/admin/login")
    public String gotToLoginPage(@ModelAttribute User user, ModelMap modelMap)
    {
        modelMap.addAttribute("user", user);
        return "elements/login";
    }
    @PostMapping("/admin/login")
    public void submitLogin(@ModelAttribute User user){
        System.out.println(user.toString());
    }

    @GetMapping("/admin/logout")
    public String gotToLogoutPage()
    {
        return "layouts/logout-layout";
    }


}
