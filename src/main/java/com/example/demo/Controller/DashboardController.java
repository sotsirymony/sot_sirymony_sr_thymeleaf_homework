package com.example.demo.Controller;

import com.example.demo.MyRepositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class DashboardController
{
    UserRepository userRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/admin/review-article")
    public String goToArticleReviewPage()
    {
        return "/layouts/article-review-layout";
    }

    @GetMapping("/admin/dashboard")
    public String goToDashboard()
    {
        return "/layouts/dashboard-layout";
    }

    @GetMapping("/home")
    public String goToHomePage()
    {
        return "/layouts/home-layout";
    }

    @GetMapping("/admin/article")
    public String goToArticlePage()
    {
        return "/layouts/article-layout";
    }

}
