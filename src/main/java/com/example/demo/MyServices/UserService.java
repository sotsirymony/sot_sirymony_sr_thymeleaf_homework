package com.example.demo.MyServices;

import com.example.demo.Entity.User;
import com.example.demo.MyRepositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

@Service
public class UserService {
    private UserRepository userRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User findUserByUsername(String username)
    {
        User user = userRepository.findUserByUsername(username);

        if(user == null)
            return null;
        return user;
    }

}


